package ua.Work3;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.collections.ExactTexts;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static java.lang.Thread.sleep;
import static org.openqa.selenium.lift.match.TextMatcher.text;

/**
 * Created by Winterfall on 03.03.2016.
 */
public class TodoMVCTest {

    @Test

    public void testTasksLifeCycle() {
        open("https://todomvc4tasj.herokuapp.com");

        add("1");
        startEdit("1", "a").pressEnter();

        //complete
        toggle("a");
        assertTasks("a");

        filterActive();
        assertNoVisibleTask();

        add("b");
        assertVisibleTasks("b");

        //complete all
        toggleAll();
        assertNoVisibleTask();

        filterCompleted();
        assertTasks("a", "b");

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a", "b");

        //reopen task
        toggle("b");
        assertVisibleTasks("a");

        clearCompleted();
        assertNoVisibleTask();

        filterAll();
        assertTasks("b");

        delete("b");
        assertNoTask();
    }

    ElementsCollection tasks = $$("#todo-list>li");

    private void add(String... tasksTexts) {
        for (String task : tasksTexts) {
            $("#new-todo").setValue(task).pressEnter();
        }
    }

    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.findBy(exactText(oldTaskText)).doubleClick();
        return tasks.findBy(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    private void clearCompleted() {
        $("#clear-completed").click();
    }

    private void toggle(String taskText) {
        tasks.findBy(exactText(taskText)).$(".toggle").click();
    }

    private void toggleAll() {
        $("#toggle-all").click();
    }

    private void assertVisibleTasks(String... tasksTexts) {
        tasks.filter(visible).shouldHave(exactTexts(tasksTexts));
    }

    private void assertTasks(String... tasksTexts) {
        tasks.shouldHave(exactTexts(tasksTexts));
    }

    private void assertNoVisibleTask() {
        tasks.filter(visible).shouldHave(empty);
    }

    private void assertNoTask() {
        tasks.shouldBe(empty);
    }

    private void filterAll() {
        $(By.linkText("All")).click();
    }

    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }
}