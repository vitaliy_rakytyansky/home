package ua.Work5;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.collections.ExactTexts;
import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;


import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.lang.Thread.sleep;
import static org.openqa.selenium.lift.match.TextMatcher.text;

/**
 * Created by Winterfall on 03.03.2016.
 */
public class TodoMVCTest extends AtTodoMVCPageWithClearedDataAfterEachTest {

    @Test
    public void testTasksLifeCycle() {

        add("1");
        startEdit("1", "a").pressEnter();

        //complete
        toggle("a");
        assertTasks("a");

        filterActive();
        assertNoVisibleTask();

        add("b");
        assertVisibleTasks("b");
        assertItemsLeft(1);

        //complete all
        toggleAll();
        assertNoVisibleTask();

        filterCompleted();
        assertTasks("a", "b");

        //reopen task
        toggle("b");
        assertVisibleTasks("a");

        clearCompleted();
        assertNoVisibleTask();

        filterAll();
        assertTasks("b");

        delete("b");
        assertNoTask();
    }

    @Test
    public void testReopenAllAtCompletedAndMoveToActive() {

        //given - tasks on completed filter
        add("a", "b");
        toggleAll();
        filterCompleted();

        toggleAll();
        assertNoVisibleTask();

        filterActive();
        assertItemsLeft(2);
    }

    @Test
    public void testCancelEditByEscAtActive() {
        add("a");
        filterActive();

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a");
        assertItemsLeft(1);
    }

    /*****************
     * Extra Coverage *
     ******************/

    @Test
    public void testEditByPressTabAtActive() {

        //given - tasks on active filter
        add("a");
        filterActive();

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(1);

    }

    @Test
    public void testEditByPressOutsideAtCompleted() {

        //given - task on completed filter
        add("a");
        toggle("a");
        filterCompleted();

        startEdit("a", "a edited");
        $("#header").click();
        assertItemsLeft(0);

    }

    @Test
    public void testEditByRemovalTextAtAll() {

        //given - tasks on all filter
        add("a", "b");

        startEdit("a", "").pressEnter();
        assertItemsLeft(1);

    }

    ElementsCollection tasks = $$("#todo-list>li");

    @Step
    private void add(String... tasksTexts) {
        for (String task : tasksTexts) {
            $("#new-todo").setValue(task).pressEnter();
        }
    }

    @Step
    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.findBy(exactText(oldTaskText)).doubleClick();
        return tasks.findBy(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    private void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    private void toggle(String taskText) {
        tasks.findBy(exactText(taskText)).$(".toggle").click();
    }

    @Step
    private void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    private void assertVisibleTasks(String... tasksTexts) {
        tasks.filter(visible).shouldHave(exactTexts(tasksTexts));
    }

    @Step
    private void assertTasks(String... tasksTexts) {
        tasks.shouldHave(exactTexts(tasksTexts));
    }

    @Step
    private void assertNoVisibleTask() {
        tasks.filter(visible).shouldHave(empty);
    }

    @Step
    private void assertNoTask() {
        tasks.shouldBe(empty);
    }

    @Step
    private void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    private void assertItemsLeft(Integer count) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(count)));
    }
}
