package ua.Work5;

import org.junit.After;
import org.junit.Before;

import java.io.IOException;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by Winterfall on 15.03.2016.
 */
public class AtTodoMVCPageWithClearedDataAfterEachTest extends BaseTest {

    @Before
    public void openPage() {
        open("https://todomvc4tasj.herokuapp.com");
    }

    @After
    public void clearData() {
        executeJavaScript("localStorage.clear()");
    }
}
