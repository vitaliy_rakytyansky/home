package ua.Work2;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.collections.ExactTexts;
import org.junit.Test;


import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.present;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static java.lang.Thread.sleep;
import static org.openqa.selenium.lift.match.TextMatcher.text;

/**
 * Created by Winterfall on 03.03.2016.
 */
public class TodoMVCTest {

    @Test

    public void testTasksLifeCycle() {
        open("https://todomvc4tasj.herokuapp.com");

        add("One", "2", "3", "4", "5");
        assertTasks("One", "2", "3", "4", "5");

        edit("1");

        //implicit verification of previous step
        toggle("1");

        //implicit verification of previous ste
        moveToFilter("Active");
        visible.shouldBe(exactTexts("2", "3", "4", "5"));

        delete("2");
        visible.shouldBe(exactTexts("3", "4", "5"));

        toggleAll();

        //implicit verification of previous step
        moveToFilter("Completed");
        assertTasks("1", "3", "4", "5");

        toggleAll();

        //implicit verification of previous step
        moveToFilter("All");
        assertTasks("1", "3", "4", "5");

        toggleAll();

        //implicit verification of previous step
        clearCompleted();
        assertNoTasks();

    }

    ElementsCollection tasks = $$("#todo-list>li");
    ElementsCollection filters = $$("#filters>li");
    ElementsCollection visible = $$("#todo-list>.active");
    SelenideElement clearButton = $("#clear-completed");


    public void add(String... taskText) {
        for (String tasks : taskText) {
            $("#new-todo").setValue(tasks).pressEnter();
        }
    }

    public void edit(String taskText) {
        tasks.get(0).doubleClick().$(".edit").setValue(taskText).pressEnter();
    }

    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    private void clearCompleted() {
        clearButton.click();
    }

    public void toggle(String taskText) {
        tasks.find(exactText(taskText)).$(".toggle").click();
    }

    public void toggleAll() {
        $("#toggle-all").click();
    }

    public void moveToFilter(String filterName) {
        filters.find(exactText(filterName)).click();
    }

    public void assertTasks(String... taskTexts) {

        tasks.shouldHave(exactTexts(taskTexts));

    }

    public void assertNoTasks() {
        tasks.shouldBe(empty);
    }


}