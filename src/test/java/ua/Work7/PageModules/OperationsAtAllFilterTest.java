package ua.Work7.PageModules;

import org.junit.Test;

import static com.codeborne.selenide.Selenide.$;
import static ua.Work7.PageModules.pages.TodoMVC.TaskType.ACTIVE;
import static ua.Work7.PageModules.pages.TodoMVC.TaskType.COMPLETED;
import static ua.Work7.PageModules.pages.TodoMVC.*;

 public class OperationsAtAllFilterTest extends BaseTest {

    @Test
     public static void testCompleteAllAtAll() {
        givenAtAll(ACTIVE, "a", "b");

        toggleAll();
        assertItemsLeft(0);
    }

    @Test
     public static void testClearCompletedAtAll() {
        given(aTask("a", COMPLETED),
                aTask("b", ACTIVE),
                aTask("c", COMPLETED));

        clearCompleted();
        assertTasks("b");
        assertItemsLeft(1);
    }

    @Test
     public static void testReopenAllAtAllAndMoveToCompleted() {
        givenAtAll(COMPLETED, "a", "b");

        toggleAll();
        assertTasks("a", "b");
        assertItemsLeft(2);

        filterCompleted();
        assertNoVisibleTask();
    }

    @Test
     public static void testCancelEditByEscAtAll() {
        given(aTask("a", ACTIVE), aTask("b", COMPLETED));

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
     public static void testEditByPressTabAtAll() {
        given(aTask("a", COMPLETED), aTask("b", ACTIVE));

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
     public static void testEditByPressOutsideAtAll() {
        givenAtAll(ACTIVE, "a");

        startEdit("a", "a edited");
        $("#header").click();
        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
     public static void testEditByRemovalTextAtAll() {
        given(aTask("a", COMPLETED), aTask("b", ACTIVE));

        startEdit("a", "").pressEnter();
        assertTasks("b");
        assertItemsLeft(1);
    }

}
