package ua.Work7.PageModules;

import org.junit.Test;

import static ua.Work7.PageModules.pages.TodoMVC.*;


public class GeneralOperationsTest extends BaseTest {

    @Test
     public static void testTasksLifeCycle() {

        givenAtAll();
        add("1");
        startEdit("1", "a").pressEnter();

        //complete
        toggle("a");
        assertTasks("a");

        filterActive();
        assertNoVisibleTask();

        add("b");
        assertVisibleTasks("b");
        assertItemsLeft(1);

        //complete all
        toggleAll();
        assertNoVisibleTask();

        filterCompleted();
        assertTasks("a", "b");

        //reopen task
        toggle("b");
        assertVisibleTasks("a");

        clearCompleted();
        assertNoVisibleTask();

        filterAll();
        assertTasks("b");

        delete("b");
        assertNoTask();
    }
}
