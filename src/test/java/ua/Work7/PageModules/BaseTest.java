package ua.Work7.PageModules;

import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;
import org.junit.After;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;

 public class BaseTest {
    @After
     public static void tearDown() throws IOException {
        screenshot();
    }

    @Attachment(type = "image/png")
     public static byte[] screenshot() throws IOException {
        File screenshot = Screenshots.takeScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }
}
