package ua.Work7.PageModules;


import org.junit.Test;

import static com.codeborne.selenide.Selenide.$;
import static ua.Work7.PageModules.pages.TodoMVC.TaskType.ACTIVE;
import static ua.Work7.PageModules.pages.TodoMVC.TaskType.COMPLETED;
import static ua.Work7.PageModules.pages.TodoMVC.*;

 public class OperationsAtCompletedFilterTest extends BaseTest {

    @Test
     public static void testAddAtCompleted() {
        givenAtCompleted(ACTIVE, "a");

        add("b");
        assertNoVisibleTask();
        assertItemsLeft(2);
    }

    @Test
     public static void testEditAtCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b", ACTIVE));

        startEdit("a", "a edit").pressEnter();
        assertVisibleTasks("a edit");
        assertItemsLeft(1);
    }

    @Test
     public static void testDeleteAtCompleted() {
        givenAtCompleted(aTask("a", ACTIVE), aTask("b", COMPLETED));

        delete("b");
        assertItemsLeft(1);
    }

    @Test
     public static void testCompleteAllAtCompleted() {
        givenAtCompleted(ACTIVE, "a", "b");

        toggleAll();
        assertItemsLeft(0);
    }

    @Test
     public static void testReopenAllAtCompletedAndMoveToActive() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b", COMPLETED));

        toggleAll();
        assertNoVisibleTask();
        assertItemsLeft(2);

        filterActive();
        assertVisibleTasks("a", "b");
    }

    @Test
     public static void testCancelEditByEscAtCompleted() {
        given(aTask("a", COMPLETED), aTask("b", COMPLETED));

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a", "b");
        assertItemsLeft(0);
    }

    @Test
     public static void testEditByPressTabAtCompleted() {
        givenAtCompleted(COMPLETED, "a", "b");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited", "b");
        assertItemsLeft(0);
    }

    @Test
     public static void testEditByPressOutsideAtCompleted() {
        givenAtCompleted(aTask("a", ACTIVE), aTask("b", COMPLETED));


        startEdit("b", "b edited");
        $("#header").click();
        assertVisibleTasks("b edited");
        assertItemsLeft(1);
    }

    @Test
     public static void testEditByRemovalTextAtCompleted() {
        givenAtCompleted(aTask("a", COMPLETED));

        startEdit("a", "").pressEnter();
        assertNoVisibleTask();
    }
}
