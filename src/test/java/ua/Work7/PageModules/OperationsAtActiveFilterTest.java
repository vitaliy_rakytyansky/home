package ua.Work7.PageModules;


import org.junit.Test;

import static com.codeborne.selenide.Selenide.$;
import static ua.Work7.PageModules.pages.TodoMVC.TaskType.ACTIVE;
import static ua.Work7.PageModules.pages.TodoMVC.TaskType.COMPLETED;
import static ua.Work7.PageModules.pages.TodoMVC.*;

 public class OperationsAtActiveFilterTest extends BaseTest {

    @Test
     public static void testEditAtActive() {
        givenAtActive(aTask("a", ACTIVE), aTask("b", COMPLETED));

        startEdit("a", "a edit").pressEnter();
        assertVisibleTasks("a edit");
        assertItemsLeft(1);
    }

    @Test
     public static void testDeleteAtActive() {
        givenAtActive(aTask("a", COMPLETED), aTask("b", ACTIVE));

        delete("b");
        assertItemsLeft(0);
    }

    @Test
     public static void testClearCompleteAtActive() {
        givenAtActive(aTask("a", ACTIVE), aTask(("b"), COMPLETED));

        clearCompleted();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
     public static void testReopenAllAtActiveAndMoveToAll() {
        givenAtActive(COMPLETED, "a", "b");

        toggleAll();
        assertItemsLeft(2);

        filterAll();
        assertTasks("a", "b");
    }

    @Test
     public static void testCancelEditByEscAtActive() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
     public static void testEditByPressTabAtActive() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
     public static void testEditByPressOutsideAtActive() {
        givenAtActive(ACTIVE, "a", "b");

        startEdit("a", "a edited");
        $("#header").click();
        assertVisibleTasks("a edited", "b");
        assertItemsLeft(2);
    }

    @Test
     public static void testEditByRemovalTextAtActive() {
        givenAtActive(ACTIVE, "a", "b");

        startEdit("a", "").pressEnter();
        assertTasks("b");
        assertItemsLeft(1);
    }

}
