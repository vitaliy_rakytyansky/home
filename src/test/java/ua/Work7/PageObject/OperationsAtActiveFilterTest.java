package ua.Work7.PageObject;


import org.junit.Test;
import ua.Work7.PageObject.pages.TodoMVCPage;

import static com.codeborne.selenide.Selenide.$;
import static ua.Work7.PageObject.pages.TodoMVCPage.TaskType.ACTIVE;
import static ua.Work7.PageObject.pages.TodoMVCPage.TaskType.COMPLETED;



public class OperationsAtActiveFilterTest extends BaseTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    public void testEditAtActive() {
        page.givenAtActive(page.aTask("a", ACTIVE), page.aTask("b", COMPLETED));

        page.startEdit("a", "a edit").pressEnter();
        page.assertVisibleTasks("a edit");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActive() {
        page.givenAtActive(page.aTask("a", COMPLETED), page.aTask("b", ACTIVE));

        page.delete("b");
        page.assertItemsLeft(0);
    }

    @Test
    public void testClearCompleteAtActive() {
        page.givenAtActive(page.aTask("a", ACTIVE), page.aTask(("b"), COMPLETED));

        page.clearCompleted();
        page.assertTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtActiveAndMoveToAll() {
        page.givenAtActive(COMPLETED, "a", "b");

        page.toggleAll();
        page.assertItemsLeft(2);

        page.filterAll();
        page.assertTasks("a", "b");
    }

    @Test
    public void testCancelEditByEscAtActive() {
        page.givenAtActive(ACTIVE, "a");

        page.startEdit("a", "a edit cancelled").pressEscape();
        page.assertTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditByPressTabAtActive() {
        page.givenAtActive(ACTIVE, "a");

        page.startEdit("a", "a edited").pressTab();
        page.assertTasks("a edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditByPressOutsideAtActive() {
        page.givenAtActive(ACTIVE, "a", "b");

        page.startEdit("a", "a edited");
        $("#header").click();
        page.assertVisibleTasks("a edited", "b");
        page.assertItemsLeft(2);
    }

    @Test
    public void testEditByRemovalTextAtActive() {
        page.givenAtActive(ACTIVE, "a", "b");

        page.startEdit("a", "").pressEnter();
        page.assertTasks("b");
        page.assertItemsLeft(1);
    }

}
