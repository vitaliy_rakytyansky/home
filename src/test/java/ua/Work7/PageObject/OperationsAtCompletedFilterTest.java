package ua.Work7.PageObject;


import org.junit.Test;
import ua.Work7.PageObject.pages.TodoMVCPage;

import static com.codeborne.selenide.Selenide.$;
import static ua.Work7.PageObject.pages.TodoMVCPage.TaskType.ACTIVE;
import static ua.Work7.PageObject.pages.TodoMVCPage.TaskType.COMPLETED;

public class OperationsAtCompletedFilterTest extends BaseTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    public void testAddAtCompleted() {
        page.givenAtCompleted(ACTIVE, "a");

        page.add("b");
        page.assertNoVisibleTask();
        page.assertItemsLeft(2);
    }

    @Test
    public void testEditAtCompleted() {
        page.givenAtCompleted(page.aTask("a", COMPLETED), page.aTask("b", ACTIVE));

        page.startEdit("a", "a edit").pressEnter();
        page.assertVisibleTasks("a edit");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtCompleted() {
        page.givenAtCompleted(page.aTask("a", ACTIVE), page.aTask("b", COMPLETED));

        page.delete("b");
        page.assertItemsLeft(1);
    }

    @Test
    public void testCompleteAllAtCompleted() {
        page.givenAtCompleted(ACTIVE, "a", "b");

        page.toggleAll();
        page.assertItemsLeft(0);
    }

    @Test
    public void testReopenAllAtCompletedAndMoveToActive() {
        page.givenAtCompleted(page.aTask("a", COMPLETED), page.aTask("b", COMPLETED));

        page.toggleAll();
        page.assertNoVisibleTask();
        page.assertItemsLeft(2);

        page.filterActive();
        page.assertVisibleTasks("a", "b");
    }

    @Test
    public void testCancelEditByEscAtCompleted() {
        page.given(page.aTask("a", COMPLETED), page.aTask("b", COMPLETED));

        page.startEdit("a", "a edit cancelled").pressEscape();
        page.assertTasks("a", "b");
        page.assertItemsLeft(0);
    }

    @Test
    public void testEditByPressTabAtCompleted() {
        page.givenAtCompleted(COMPLETED, "a", "b");

        page.startEdit("a", "a edited").pressTab();
        page.assertTasks("a edited", "b");
        page.assertItemsLeft(0);
    }

    @Test
    public void testEditByPressOutsideAtCompleted() {
        page.givenAtCompleted(page.aTask("a", ACTIVE), page.aTask("b", COMPLETED));


        page.startEdit("b", "b edited");
        $("#header").click();
        page.assertVisibleTasks("b edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditByRemovalTextAtCompleted() {
        page.givenAtCompleted(page.aTask("a", COMPLETED));

        page.startEdit("a", "").pressEnter();
        page.assertNoVisibleTask();
    }
}
