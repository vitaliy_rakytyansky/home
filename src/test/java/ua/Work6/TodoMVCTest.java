package ua.Work6;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;

import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static ua.Work6.TodoMVCTest.TaskType.*;

/**
 * Created by Winterfall on 03.03.2016.
 */
public class TodoMVCTest extends ScreenshotAfterEachTest {

    @Test
    public void testTasksLifeCycle() {

        givenAtAll();
        add("1");
        startEdit("1", "a").pressEnter();

        //complete
        toggle("a");
        assertTasks("a");

        filterActive();
        assertNoVisibleTask();

        add("b");
        assertVisibleTasks("b");
        assertItemsLeft(1);

        //complete all
        toggleAll();
        assertNoVisibleTask();

        filterCompleted();
        assertTasks("a", "b");

        //reopen task
        toggle("b");
        assertVisibleTasks("a");

        clearCompleted();
        assertNoVisibleTask();

        filterAll();
        assertTasks("b");

        delete("b");
        assertNoTask();
    }

    @Test
    public void testAddAtCompleted() {
        givenAtCompleted(ACTIVE, "a");

        add("b");
        assertNoVisibleTask();
        assertItemsLeft(2);
    }

    @Test
    public void testEditAtActive() {
        givenAtActive(aTask("a", ACTIVE), aTask("b", COMPLETED));

        startEdit("a", "a edit").pressEnter();
        assertVisibleTasks("a edit");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAtCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b", ACTIVE));

        startEdit("a", "a edit").pressEnter();
        assertVisibleTasks("a edit");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActive() {
        givenAtActive(aTask("a", COMPLETED), aTask("b", ACTIVE));

        delete("b");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteAtCompleted() {
        givenAtCompleted(aTask("a", ACTIVE), aTask("b", COMPLETED));

        delete("b");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteAllAtAll() {
        givenAtAll(ACTIVE, "a", "b");

        toggleAll();
        assertItemsLeft(0);
    }

    @Test
    public void testCompleteAllAtCompleted() {
        givenAtCompleted(ACTIVE, "a", "b");

        toggleAll();
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompletedAtAll() {
        given(aTask("a", COMPLETED),
                aTask("b", ACTIVE),
                aTask("c", COMPLETED));

        clearCompleted();
        assertTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testClearCompleteAtActive() {
        givenAtActive(aTask("a", ACTIVE), aTask(("b"), COMPLETED));

        clearCompleted();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenAllAtAllAndMoveToCompleted() {
        givenAtAll(COMPLETED, "a", "b");

        toggleAll();
        assertTasks("a", "b");
        assertItemsLeft(2);

        filterCompleted();
        assertNoVisibleTask();
    }

    @Test
    public void testReopenAllAtActiveAndMoveToAll() {
        givenAtActive(COMPLETED, "a", "b");

        toggleAll();
        assertItemsLeft(2);

        filterAll();
        assertTasks("a", "b");
    }

    @Test
    public void testReopenAllAtCompletedAndMoveToActive() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b", COMPLETED));

        toggleAll();
        assertNoVisibleTask();
        assertItemsLeft(2);

        filterActive();
        assertVisibleTasks("a", "b");
    }

    @Test
    public void testCancelEditByEscAtActive() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditByEscAtAll() {
        given(aTask("a", ACTIVE), aTask("b", COMPLETED));

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditByEscAtCompleted() {
        given(aTask("a", COMPLETED), aTask("b", COMPLETED));

        startEdit("a", "a edit cancelled").pressEscape();
        assertTasks("a", "b");
        assertItemsLeft(0);
    }

    @Test
    public void testEditByPressTabAtActive() {
        givenAtActive(ACTIVE, "a");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditByPressTabAtAll() {
        given(aTask("a", COMPLETED), aTask("b", ACTIVE));

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditByPressTabAtCompleted() {
        givenAtCompleted(COMPLETED, "a", "b");

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited", "b");
        assertItemsLeft(0);
    }

    @Test
    public void testEditByPressOutsideAtAll() {
        givenAtAll(ACTIVE, "a");

        startEdit("a", "a edited");
        $("#header").click();
        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditByPressOutsideAtActive() {
        givenAtActive(ACTIVE, "a", "b");

        startEdit("a", "a edited");
        $("#header").click();
        assertVisibleTasks("a edited", "b");
        assertItemsLeft(2);
    }

    @Test
    public void testEditByPressOutsideAtCompleted() {
        givenAtCompleted(aTask("a", ACTIVE), aTask("b", COMPLETED));


        startEdit("b", "b edited");
        $("#header").click();
        assertVisibleTasks("b edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditByRemovalTextAtAll() {
        given(aTask("a", COMPLETED), aTask("b", ACTIVE));

        startEdit("a", "").pressEnter();
        assertTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditByRemovalTextAtActive() {
        givenAtActive(ACTIVE, "a", "b");

        startEdit("a", "").pressEnter();
        assertTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditByRemovalTextAtCompleted() {
        givenAtCompleted(aTask("a", COMPLETED));

        startEdit("a", "").pressEnter();
        assertNoVisibleTask();
    }

    ElementsCollection tasks = $$("#todo-list>li");

    @Step
    private void add(String... tasksTexts) {
        for (String task : tasksTexts) {
            $("#new-todo").shouldBe(enabled).setValue(task).pressEnter();
        }
    }

    @Step
    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.findBy(exactText(oldTaskText)).doubleClick();
        return tasks.findBy(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    private void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    private void toggle(String taskText) {
        tasks.findBy(exactText(taskText)).$(".toggle").click();
    }

    @Step
    private void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    private void assertVisibleTasks(String... tasksTexts) {
        tasks.filter(visible).shouldHave(exactTexts(tasksTexts));
    }

    @Step
    private void assertTasks(String... tasksTexts) {
        tasks.shouldHave(exactTexts(tasksTexts));
    }

    @Step
    private void assertNoVisibleTask() {
        tasks.filter(visible).shouldHave(empty);
    }

    @Step
    private void assertNoTask() {
        tasks.shouldBe(empty);
    }

    @Step
    private void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    private void assertItemsLeft(Integer count) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(count)));
    }

    //==============
    //Helpers Block
    //==============

    public enum TaskType {
        COMPLETED("true"), ACTIVE("false");

        private String flag;

        TaskType(String flag) {
            this.flag = flag;
        }

        public String getFlag() {
            return flag;
        }
    }

    public class Task {
        String taskText;
        TaskType taskType;

        public Task(String taskText, TaskType taskType) {
            this.taskText = taskText;
            this.taskType = taskType;
        }
    }

    public Task aTask(String taskText, TaskType taskType) {
        return new Task(taskText, taskType);
    }

    public void given(Task... tasks) {
        if (!(url().equals("https://todomvc4tasj.herokuapp.com/")))
            open("https://todomvc4tasj.herokuapp.com/");

        String elements = "";
        if (tasks.length != 0) {
            for (Task task : tasks) {
                elements += "{\\\"completed\\\":" + task.taskType.getFlag() + ", \\\"title\\\":\\\"" + task.taskText + "\\\"},";
            }
            elements = elements.substring(0, elements.length() - 1);
        }
        executeJavaScript("localStorage.setItem(\"todos-troopjs\", \"[" + elements + "]\")");
        refresh();
    }

    public void givenAtAll(Task... tasks) {
        given(tasks);
    }

    public void givenAtActive(Task... tasks) {
        given(tasks);
        filterActive();
    }

    public void givenAtCompleted(Task... tasks) {
        given(tasks);
        filterCompleted();
    }

    public void givenAtAll(TaskType taskType, String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = new Task(taskTexts[i], taskType);
        }
        given(tasks);
    }

    public void givenAtActive(TaskType taskType, String... taskTexts) {
        givenAtAll(taskType, taskTexts);
        filterActive();
    }

    public void givenAtCompleted(TaskType taskType, String... taskTexts) {
        givenAtAll(taskType, taskTexts);
        filterCompleted();
    }

}
